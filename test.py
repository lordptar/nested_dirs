#!/usr/bin/python3

from modules.functions import *

template_file = 'devops-scripting-test-codes.txt'
deploy_dir = 'deploy_dir_python/'
template_link = 'https://gist.githubusercontent.com/kwijibo/6d136a098f0b6e77793c/raw/0e9de8ceb52e4bde9574a9e6ebc4fbb5aeca1d79/devops-scripting-test-codes.txt'


check_template_file(template_file, template_link)
check_deploy_dir(deploy_dir)
test_list = make_list(template_file)

for row in test_list:
    base_dir = (deploy_dir + '/'.join(row[:3]))
    file_path = (base_dir + "/" + '-'.join(row) + ".mov")
    make_dir(base_dir)
    create_file(file_path)
