Task:

You have a list of linebreak-separated codes 

Each code is of the format: ABC-DEF-GHI-JKLM-123-001

Write a script that reads the codes, and creates a directory structure based on the first four hyphen-separated components of the codes.

Create an empty file for each code at the appropriate place in the directory structure, and give it a .mov extension.

eg: ABC-DEF-GHI-JKLM-123-001 should result in:

      /DEF
        /GHI
          /JKLM
            ABC-DEF-GHI-JKLM-123-001.mov

Use an online version control repository such as Github or Bitbucket to publish your script (and any ancillary files), and email us with the URL.


Solution:

To complete the task  I created two programs, one is written in Bash, the other in Python. Both programs have the same functionality.

The algorithm of the programs:
1. Check if the local version of the template file (devops-scripting-test-codes.txt) exists. If there is no local file, the program gives you the choice to download the file or exit from the program
2. Check if the deployment directory exists. If the directory exists the program gives you the choice to redeploy or exit from the program
3. Create the structure of folders and create empty files.