#!/usr/bin/python3

import os
import shutil
import urllib.request


def make_list(file_name):
    try:
        with open(file_name) as test_file:
            return([every_line.strip().split('-') for every_line in test_file])
    except IOError as ioerr:
            print('IO errors: ' + str(ioerr))
            return(None)


def make_dir(path):
    try:
        os.makedirs(path)
    except OSError as err:
        print('OS errors: ' + str(err))
        return(None)


def create_file(file_path):
    try:
        open(file_path, 'a').close()
    except IOError as ioerr:
            print('IO errors: ' + str(ioerr))
            return(None)


def check_deploy_dir(path):
    if os.path.exists(path):
        print("The deployment directory already exists")
        d_answer = input('To redeploy press "y" or press "Enter":  ')
        if d_answer == "y":
            shutil.rmtree(path)
        else:
            quit()


def check_template_file(path, url):
    if not os.path.isfile(path):
        print("You don't have the local version of the template file")
        print("Would you like to download the file?")
        f_answer = input('For yes press "y" and to exit press "Enter": ')
        if f_answer == "y":
            urllib.request.urlretrieve(url, path)
        else:
            quit()
