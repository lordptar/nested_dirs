#!/bin/bash

set -e 

# Set up variables 
template_file="devops-scripting-test-codes.txt"
deploy_dir="deploy_dir_bash"
template_link="https://gist.githubusercontent.com/kwijibo/6d136a098f0b6e77793c/raw/0e9de8ceb52e4bde9574a9e6ebc4fbb5aeca1d79/devops-scripting-test-codes.txt"

# Check and if necessary download the template file or exit
if [ ! -f  $template_file ] 
    then
    echo "You don't have the local version of the template file"
    echo "Would you like to download the file?"
    echo -n 'For yes press "y" and to exit press any key: '
    read -n 1 f_answer
    echo
    [[ $f_answer == "y" ]] && wget $template_link || exit 0
fi

# Check and if necessary remove the deployment directory or exit
if [ -d  $deploy_dir ] 
    then
    echo "The deployment directory already exists"
    echo -n 'To redeploy press "y" or any key to exit: '
    read -n 1 d_answer
    echo
    [[ $d_answer == "y" ]] && rm -rf $deploy_dir || exit 0
fi

# The deployment 
awk -F - "{system(\"install -D /dev/null $deploy_dir/\"\$1\"/\"\$2\"/\"\$3\"/\"\$0\".mov\")}" $template_file
echo
echo "The deployment has been completed successfully"
